package main

import (
    "log"
    "flag"

    "bitbucket.org/lmika/binpkg/pkgs"
    "bitbucket.org/lmika/binpkg/tasks"
    "bitbucket.org/lmika/binpkg/pkgplan"
)

func main() {
    flag.Parse()
    repo := &pkgs.FileRepository{"_pkgrepo"}

    dr := pkgs.NewDependencyResolver(repo)

    //pkgs := make([]*pkgs.Package, flag.NArg())
    for _, pkgName := range flag.Args() {
        err := dr.Add(pkgName)
        if err != nil {
            log.Fatal(err)
        }
    }

    dr.Println()

    // The context
    ctx := &tasks.Context{
        DryRun: false,
        Dirs: tasks.ContextDirs{
            Include: "/MinGW/include",
            Lib: "/MinGW/lib",
            Bin: "/MinGW/bin",
            PkgConfig: "/MinGW/lib/pkgconfig",
        },
        Props: make(map[string]string),
    }

    // Builds the task list
    pkgs := dr.ToSlice()
    taskList, err := pkgplan.BuildInstallTasks(pkgs)
    if err != nil {
        log.Fatal(err)
    }

    // Execute the tasks
    err = taskList.Do(ctx)
    if err != nil {
        log.Fatal(err)
    }
}