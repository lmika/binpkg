package pkgplan

import (
    "path"
    "fmt"
    "strings"

    "bitbucket.org/lmika/binpkg/pkgs"
    "bitbucket.org/lmika/binpkg/tasks"
)

// Builds a task list installing the packages 
func BuildInstallTasks(pkg []*pkgs.Package) (tasks.TaskList, error) {
    var err error
    tasks := tasks.TaskList(make([]tasks.Task, 0))

    // Add all the download tasks
    for _, p := range pkg {
        tasks, err = addDownloadTasks(tasks, p)
        if err != nil {
            return nil, err
        }
    }

    // Add the deployment tasks
    for _, p := range pkg {
        tasks, err = addMin32PkgInstalls(tasks, p)
        if err != nil {
            return nil, err
        }

        tasks, err = addDeployTasks(tasks, p)
        if err != nil {
            return nil, err
        }
    }

    return tasks, err
}

// Add min32 package installs
func addMin32PkgInstalls(tl tasks.TaskList, pkg *pkgs.Package) (tasks.TaskList, error) {
    for _, min32Pkg := range pkg.Min32Pkgs {
        tl = append(tl, &tasks.RunCmdTask{[]string { "mingw-get", "install", min32Pkg }})
    }
    return tl, nil
}

// Adds the download tasks to the passed in task list.  This will download and unzip the archive.
// After this task is executed, the following packaged properties will be set:
//
//      <pkgPrefix>_archive:        Base directory of the unzipped archive
//
func addDownloadTasks(tl tasks.TaskList, pkg *pkgs.Package) (tasks.TaskList, error) {
    // If there's no archive, don't do anything
    if pkg.Archive == "" {
        return tl, nil
    }

    // TODO: Generate a temporary name for where the files should live
    downloadedFile := "/tmp/" + path.Base(pkg.Archive)
    unzippedArchiveDir := fmt.Sprintf("/tmp/%s-%s", pkg.Name, pkg.Version) 

    tl = append(tl, &tasks.GetTask{pkg.Archive, downloadedFile})
    tl = append(tl, &tasks.ExtractTask{downloadedFile, unzippedArchiveDir})

    // TODO: Set the property for where the archive is going to live
    propertyName := pkgProp(pkg, "archive")
    tl = append(tl, &tasks.SetPropTask{propertyName, unzippedArchiveDir})

    return tl, nil
}

// Add the tasks that copy the artefacts to their final destination.  This requires the following properties
//
//      <pkgPrefix>_archive
//
func addDeployTasks(tl tasks.TaskList, pkg *pkgs.Package) (tasks.TaskList, error) {
    srcPrefix := fmt.Sprintf("{{.Props.%s}}", pkgProp(pkg, "archive"))

    for _, fs := range pkg.Deploy {
        tl = append(tl, &tasks.CopyTask{path.Join(srcPrefix, fs.Src), fs.Target})
    }

    return tl, nil
}


// Return a package-local property name
func pkgProp(pkg *pkgs.Package, name string) string {
    pkgName := strings.Replace(pkg.Name, "-", "_", -1)

    return fmt.Sprintf("__%s_%s", pkgName, name)
}