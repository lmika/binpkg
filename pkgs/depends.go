package pkgs

import (
    "errors"
    "log"
)

// A node in a dependency tree
type DependencyNode struct {
    Pkg         *Package
    Deps        []*DependencyNode
}

// Displays the dependency graph
func (dn *DependencyNode) Print(ident string) {
    if dn.Pkg != nil {
        log.Println(ident + dn.Pkg.Name)
        ident += ". "
    }

    for _, deps := range dn.Deps {
        if deps != nil {
            deps.Print(ident)
        }
    }
}

// Adds the dependency nodes to the slice in post-traversal
func (dn *DependencyNode) AddToSlice(slice []*Package) []*Package {
    for _, deps := range dn.Deps {
        if deps != nil {
            slice = deps.AddToSlice(slice)
        }
    }

    if dn.Pkg != nil {
        slice = append(slice, dn.Pkg)
    }

    return slice
}



// Dependency resolver
type DependencyResolver struct {
    pkgSet      map[string]*Package
    rootNode    *DependencyNode
    repo        Repository
}

// Returns a new dependency resolver
func NewDependencyResolver(repo Repository) *DependencyResolver {
    return &DependencyResolver{
        pkgSet: make(map[string]*Package),
        rootNode: &DependencyNode{nil, make([]*DependencyNode, 0)},
        repo: repo,
    }
}

// Get the added packages as a slice
func (dr *DependencyResolver) ToSlice() []*Package {
    return dr.rootNode.AddToSlice(make([]*Package, 0))
}

// Adds a package to the top level
func (dr *DependencyResolver) Add(pkgName string) error {
    depNode, err := dr.add(pkgName)
    if err != nil {
        return err
    }

    dr.rootNode.Deps = append(dr.rootNode.Deps, depNode)
    return nil
}

func (dr *DependencyResolver) Println() {
    dr.rootNode.Print("")
}


// Adds a package and it's dependencies.  Dependency cycles are not
// permitted.
func (dr *DependencyResolver) add(pkgName string) (*DependencyNode, error) {
    _, pkgFound := dr.pkgSet[pkgName]
    if pkgFound {
        return nil, errors.New("Dependency cyle: " + pkgName)
    }

    // Fetch the package
    pkg, err := dr.repo.Fetch(pkgName)
    if err != nil {
        return nil, errors.New(pkgName + ": " + err.Error())
    }

    // Fetch all the dependencies
    dr.pkgSet[pkgName] = pkg
    pkgDeps := make([]*DependencyNode, 0)
    for subPkg, _ := range pkg.Depends {
        subPkgNode, err := dr.add(subPkg)
        if err != nil {
            return nil, err
        }

        pkgDeps = append(pkgDeps, subPkgNode)
    }

    return &DependencyNode{pkg, pkgDeps}, nil
}