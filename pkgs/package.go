package pkgs

import (
    "io"
    "encoding/json"
)

// Package description.  Each package describes the binary release of a
// go dependency that needs to be downloaded, configured and installed.
type Package struct {
    // The name of the package
    Name        string              `json:"name"`

    // The version of the package
    Version     string              `json:"version"`

    // Dependencies
    Depends     map[string]string   `json:"depends"`

    // Min32 packages
    Min32Pkgs   []string            `json:"mingw32-packages"`
    
    // The URL of the binary release of the package
    Archive     string              `json:"archive"`

    // A 'pkgconfig' manifest which will be generated when releasing the package.
    PkgConfig   map[string]string   `json:"pkgconfig"`

    // The set of files to deploy
    Deploy      []*FileCopySet      `json:"deploy"`
}

// A set of files
type FileCopySet struct {
    Src         string              `json:"src"`
    Target      string              `json:"target"`
}


// Reads a package from a reader
func ReadPackage(r io.Reader) (*Package, error) {
    dec := json.NewDecoder(r)
    pkg := &Package{}
    err := dec.Decode(pkg)
    return pkg, err
}