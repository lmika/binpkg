package pkgs

import (
    "os"
    "path/filepath"
)

// A package repository
type Repository interface {
    
    // Fetches a package by it's package name.  Returns the package manifest if one
    // is found, or an error if there was an error fetching the package.
    Fetch(name string) (*Package, error)
}


// A file-based package repository stored on the local file system
type FileRepository struct {
    BaseDir     string
}

func (fr *FileRepository) Fetch(name string) (*Package, error) {
    pkgFile := filepath.Join(fr.BaseDir, name + ".json")
    file, err := os.Open(pkgFile)
    if err != nil {
        return nil, err
    }
    defer file.Close()

    return ReadPackage(file)
}