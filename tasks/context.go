package tasks

import (
    "bytes"
    "text/template"
)

// The task execution context.
type Context struct {
    // True if the task is only to print what it is about to do
    DryRun      bool

    // Directories
    Dirs        ContextDirs

    // Properties
    Props       map[string]string
}


// Evaluate the string as a template using the context as the template data
func (ctx *Context) Eval(s string) string {
    t := template.Must(template.New(s).Parse(s))
    bfr := &bytes.Buffer{}

    t.Execute(bfr, ctx)

    return bfr.String()
}


// Context directories
type ContextDirs struct {
    // Include directory (e.g. /usr/include)
    Include     string

    // Lib directory    (e.g. /usr/lib)
    Lib         string

    // Bin directory    (e.g. /bin/dir)
    Bin         string

    // Pkgconfig directory
    PkgConfig   string
}