package tasks

import (
    "os"
)

// Copies files or directories from a source to a destination
type CopyTask struct {
    Src             string
    Dest            string
}

func (t *CopyTask) Do(ctx *Context) error {
    src := ctx.Eval(t.Src)
    dest := ctx.Eval(t.Dest)

    // If the source is a single file, simply copy the file
    srcInfo, err := os.Stat(src)
    if err != nil {
        return err
    }

    if srcInfo.IsDir() {
        return copyDir(ctx, src, dest)
    } else {
        return copyFile(ctx, src, dest)
    }
}
