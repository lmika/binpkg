package tasks

import (
    "log"
)

// Task which will set the value of a property
type SetPropTask struct {
    Name        string
    Value       string
}

func (t *SetPropTask) Do(ctx *Context) error {
    name := ctx.Eval(t.Name)
    value := ctx.Eval(t.Value)

    log.Printf("Setting property '%s' to '%s'\n", name, value)
    ctx.Props[name] = value

    return nil
}