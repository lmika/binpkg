package tasks

import (
    "io"
    "io/ioutil"
    "bytes"
    "errors"
    "fmt"
    "net/url"
    "net/http"
)

// Fetches a resource identified by a URL
func fetchResource(ctx *Context, u string) (io.ReadCloser, error) {
    getUrl, err := url.Parse(u)
    if err != nil {
        return nil, err
    }

    fetchFn, hasFetcherFn := fetcherFns[getUrl.Scheme]
    if !hasFetcherFn {
        return nil, errors.New("Unrecognised scheme: " + getUrl.Scheme)
    }

    if ctx.DryRun {
        return ioutil.NopCloser(&bytes.Buffer{}), nil
    }

    return fetchFn(getUrl)
}



// Function used to fetch the resource based on the scheme of the URL
type fetcherFn      func(u *url.URL) (io.ReadCloser, error)

// Fetch a reader from HTTP
func fetchHttp(u *url.URL) (io.ReadCloser, error) {
    resp, err := http.Get(u.String())
    if err != nil {
        return nil, err
    }

    if resp.StatusCode != 200 {
        return nil, errors.New(fmt.Sprintf("HTTP Error %d", resp.StatusCode))
    }

    return resp.Body, nil
}

// The set of supported fetcher functions
var fetcherFns = map[string]fetcherFn {
    "http": fetchHttp,
    "https": fetchHttp,
}