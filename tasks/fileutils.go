package tasks

import (
    "io"
    "io/ioutil"
    "os"
    "log"
    "path/filepath"
    "strings"
)


// Opens a file for writing.  If the target directory does not exist, it will
// be created.  If the context is in dry-run mode, the physical
// file is not actually opened, instead an in-memory buffer is used.
func writeFile(ctx *Context, dest string, writer func(w io.Writer) error) error {
    extDest := ctx.Eval(dest)

    if (ctx.DryRun) {
        return writer(ioutil.Discard)
    } else {
        if err := os.MkdirAll(filepath.Dir(extDest), os.FileMode(0755)) ; err != nil {
            return err
        }

        if file, err := os.Create(extDest) ; err != nil {
            return err
        } else {
            defer file.Close()
            file.Chmod(0744)

            return writer(file)
        }
    }
}

// Returns true if the file should be copied
func shouldCopyFile(file string) bool {
    return !(strings.HasPrefix(filepath.Base(file), "."))
}

// Copies a single file.  This ensures that the destination directory exists.
func copyFile(ctx *Context, src string, dest string) error {
    if !shouldCopyFile(src) {
        return nil
    }

    log.Printf("cp %s -> %s\n", src, dest)

    if ctx.DryRun {
        return nil
    }

    finalFilename := dest
    destStats, _ := os.Stat(dest)

    // If the destination is an existing directory, copy the file into the directory
    // Otherwise, assume the destination is a new file.
    if (destStats != nil) && (destStats.IsDir()) {
        finalFilename = filepath.Join(dest, filepath.Base(src))
    } else {
        os.MkdirAll(filepath.Dir(dest), os.FileMode(0755))
    }

    // Copy the file
    destFile, err := os.Create(finalFilename)
    if err != nil {
        return err
    }
    defer destFile.Close()

    srcFile, err := os.Open(src)
    if err != nil {
        return err
    }
    defer srcFile.Close()

    _, err = io.Copy(destFile, srcFile)
    return err
}

// Recursively copies a directory.  src must be a directory
func copyDir(ctx *Context, src string, dest string) error {
    if !shouldCopyFile(src) {
        return nil
    }
    
    fileInfos, err := ioutil.ReadDir(src)
    if err != nil {
        return err
    }

    for _, fileInfo := range fileInfos {
        nestSrc := filepath.Join(src, fileInfo.Name())
        nestDest := filepath.Join(dest, fileInfo.Name())

        if (fileInfo.IsDir()) {
            copyDir(ctx, nestSrc, nestDest)
        } else {
            copyFile(ctx, nestSrc, nestDest)
        }
    }
    return nil
}