package tasks

import (
    "io"
    "log"
)


// Retrieves a resource from a URL and copies it to the destination
type GetTask struct {
    Url     string
    Dest    string
}

func (t *GetTask) Do(ctx *Context) error {
    u := ctx.Eval(t.Url)
    d := ctx.Eval(t.Dest)

    log.Printf("get %s -> %s\n", u, d)

    rc, err := fetchResource(ctx, u)
    if err != nil {
        return err
    }
    defer rc.Close()

    return writeFile(ctx, d, func(fw io.Writer) error {
        _, err := io.Copy(fw, rc)
        return err
    })
}
