package tasks

import (
    "log"
    "io"
    "fmt"
)

// Generates a package config file.
type PkgConfigTask struct {
    // The filename of the pkgconfig file
    FileName    string

    // The contents of the pkgconfig file
    Content     map[string]string
}

func (t *PkgConfigTask) Do(ctx *Context) error {
    log.Println("Writing pkgconfig file at ", ctx.Eval(t.FileName))

    return writeFile(ctx, t.FileName, func(w io.Writer) error {
        for k, v := range t.Content {
            fmt.Fprintf(w, "%s: %s\n", k, ctx.Eval(v))
        }
        return nil
    })
}