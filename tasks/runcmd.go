package tasks

import (
    "log"
    "os"
    "os/exec"
    "strings"
)

// Run a command
type RunCmdTask struct {
    CmdArgs         []string
}

func (t *RunCmdTask) Do(ctx *Context) error {
    log.Println(strings.Join(t.CmdArgs, " "))

    cmd := exec.Command(t.CmdArgs[0], t.CmdArgs[1:]...)
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr
    return cmd.Run()
}
