package tasks

import (
    "log"
)

// A list of tasks.  This will execute the tasks until an error is encountered
type TaskList   []Task

func (tl TaskList) Do(ctx *Context) error {
    for _, t := range tl {
        log.Println(t)
        err := t.Do(ctx)
        if err != nil {
            return err
        }
    }

    return nil
}