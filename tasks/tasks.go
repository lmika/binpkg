package tasks

// A task to execute
type Task interface {
    
    // Performs the task within the given context.  Returns an error
    // if one is encountered.
    Do(ctx *Context) error
}