package tasks

import (
    "io"
    "log"
    "path/filepath"
)

// Unzips files into a (new) directory
type ExtractTask struct {
    Archive     string
    Dest        string
}

func (t *ExtractTask) Do(ctx *Context) error {
    archive := ctx.Eval(t.Archive)
    dest := ctx.Eval(t.Dest)

    log.Printf("Extracting %s -> %s\n", archive, dest)

    return readArchiveFile(ctx, archive, func(name string, r io.Reader) error {
        fullName := filepath.Join(dest, name)
        log.Printf(" .. %s\n", fullName)
        
        return writeFile(ctx, fullName, func(w io.Writer) error {
            _, err := io.Copy(w, r)
            return err
        })
    })
}