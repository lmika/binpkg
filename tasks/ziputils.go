package tasks

import (
    "archive/zip"
    "archive/tar"

    "compress/gzip"
    "compress/bzip2"

    "errors"
    "path/filepath"
    "io"
    "io/ioutil"
    "os"
)


// Reads the contents of an archive.  This will open the archive and call "eachEntry"
// for every entry read.  The entry name will be the full path of the entry
func readArchiveFile(ctx *Context, file string, eachEntry func(name string, r io.Reader) error) error {
    ext := filepath.Ext(file)
    reader, hasReader := archiveReaders[ext]
    if !hasReader {
        return errors.New("No archive reader for extension: " + ext)
    }

    return reader(file, eachEntry)
}

// Reads a zip archive
func readZipArchive(file string, eachEntry func(name string, r io.Reader) error) error {
    r, err := zip.OpenReader(file)
    if err != nil {
        return err
    }
    defer r.Close()

    for _, f := range r.File {
        fi := f.FileInfo()

        if !fi.IsDir() {
            rc, err := f.Open()
            if err != nil {
                return err
            }
            
            err = eachEntry(f.Name, rc)
            rc.Close()

            if err != nil {
                return err
            }
        }
    }

    return nil
}

// Reads a tar archive from a reader
func readTarArchive(ar io.Reader, eachEntry func(name string, r io.Reader) error) error {
    tr := tar.NewReader(ar)
    for {
        hdr, err := tr.Next()
        if err == io.EOF {
            break
        } else if err != nil {
            return err
        }

        fi := hdr.FileInfo()
        if !fi.IsDir() {
            err = eachEntry(hdr.Name, tr)
            if err != nil {
                return err
            }
        }
    }

    return nil
}

// Returns a new archiveReader for a tar file wrapped in a compressed format
func wrappedTarArchiveReader(readFactory func(r io.Reader) (io.ReadCloser, error)) archiveReader {
    return func(file string, eachEntry func(name string, r io.Reader) error) error {
        fr, err := os.Open(file)
        if err != nil {
            return err
        }
        defer fr.Close()

        var ar io.ReadCloser = fr
        if readFactory != nil {
            ar, err = readFactory(ar)
            if err != nil {
                return err
            }
            defer ar.Close()
        }

        return readTarArchive(ar, eachEntry)
    }
}

// An archive reader
type archiveReader    func(file string, eachEntry func(name string, r io.Reader) error) error

// Archive readers based on the extension of the archive
var archiveReaders = map[string]archiveReader {
    ".zip": readZipArchive,

    // Plain tar archive
    ".tar": wrappedTarArchiveReader(nil),

    // GZipped tar archive
    ".gz": wrappedTarArchiveReader(func(r io.Reader) (io.ReadCloser, error) {
        return gzip.NewReader(r)
    }),
    ".tgz": wrappedTarArchiveReader(func(r io.Reader) (io.ReadCloser, error) {
        return gzip.NewReader(r)
    }),

    // BZip2
    ".bz2": wrappedTarArchiveReader(func(r io.Reader) (io.ReadCloser, error) {
        return ioutil.NopCloser(bzip2.NewReader(r)), nil
    }),
}